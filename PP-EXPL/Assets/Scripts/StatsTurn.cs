﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class StatsTurn : MonoBehaviour, IComparable {

    //public static UnitStats Instance;

    float actualspeed;
    
    public int nextActTurn;

	public bool dead = false;

    void Start()
    {

    }

	public bool isDead() {
		return this.dead;
	}

    public void calculateNextActTurn(int currentTurn)
    {
        if (GetComponent<UnitStats>() != null)
        {
            actualspeed = GetComponent<UnitStats>().localPlayerData.speed;
        }
        else if (GetComponent<EnemyStats>() != null)
        {
            actualspeed = GetComponent<EnemyStats>().speed;
        }
        this.nextActTurn = currentTurn + (int)Math.Ceiling(100.0f / actualspeed);
        //ownerStats = this.owner.GetComponent<UnitStats>().localPlayerData;
    }

    public int CompareTo(object otherStats)
    {
        return nextActTurn.CompareTo(((StatsTurn)otherStats).nextActTurn);
    }

}

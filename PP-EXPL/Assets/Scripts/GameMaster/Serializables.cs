﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class PlayerStatistics
{
    public float health;
    public float mana;
    public float attack;
    public float currentExperience;
    public float magic;
    public float defense;
    public float speed;
}

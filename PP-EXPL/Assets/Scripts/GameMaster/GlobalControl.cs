﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GlobalControl : MonoBehaviour
{
    public static GlobalControl Instance;

    public PlayerStatistics savedPlayerData = new PlayerStatistics();
    public PlayerStatistics savedPlayerData2 = new PlayerStatistics();

    public Vector3 plPos;
    public Quaternion plRot;
    public Vector3 plScl;

    public GameObject Player;

    public Transform TransitionTarget;

    void Awake()
    {
        Application.targetFrameRate = 144;

        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        if (TransitionTarget == null)
            TransitionTarget = gameObject.transform;

        plPos = GameObject.Find("Player").transform.position;
        plRot = GameObject.Find("Player").transform.rotation;
        plScl = GameObject.Find("Player").transform.localScale;
    }

    public List<string> unityGameObjects = new List<string>();

    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        foreach (string objet in unityGameObjects) {
            Destroy(GameObject.Find(objet));
        }
        if (scene.name != "Battle") {
            GameObject.Find("Player").transform.position = plPos;
            GameObject.Find("Player").transform.rotation = plRot;
            GameObject.Find("Player").transform.localScale = plScl;
        }
    }

}

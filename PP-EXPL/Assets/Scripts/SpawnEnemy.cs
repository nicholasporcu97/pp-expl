﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SpawnEnemy : MonoBehaviour {

    GlobalControl globalControl;

    [SerializeField]
	private GameObject enemyEncounterPrefab;

	public bool spawning = false;

	void Start() {
		DontDestroyOnLoad (this.gameObject);

        globalControl = GameObject.Find("GameMaster").GetComponent<GlobalControl>();

        SceneManager.sceneLoaded += OnSceneLoaded;
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		if (scene.name == "Battle") {
			if (this.spawning) {
				Instantiate (enemyEncounterPrefab);
			}
			SceneManager.sceneLoaded -= OnSceneLoaded;
            AddSomethingToList();
            Destroy (this.gameObject);
		}
	}

    bool collided;

    IEnumerator OnTriggerEnter(Collider other) {
        collided = true;
        yield return new WaitForSeconds(0.001f);
        if (collided & other.gameObject.tag == "Player") {
            this.spawning = true;
            SavePosition();
            SceneManager.LoadScene("Battle");
        }
	}

    void OnCollisionExit() {
        collided = false;
    }

    public void SavePosition() {
        GameObject player = GameObject.Find("Player");
        globalControl.plPos = player.transform.position;
        globalControl.plRot = player.transform.rotation;
        globalControl.plScl = player.transform.localScale;
    }

    void AddSomethingToList() {
        if (!globalControl.unityGameObjects.Contains(this.gameObject.name))
            globalControl.unityGameObjects.Add(this.gameObject.name);
    }
}

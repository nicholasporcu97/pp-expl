﻿using UnityEngine;
using System.Collections;

public class AttackTarget : MonoBehaviour {

	public GameObject owner;

    public PlayerStatistics ownerStats = new PlayerStatistics();
    public PlayerStatistics targetStats = new PlayerStatistics();

    [SerializeField]
	private string attackAnimation;

	[SerializeField]
	private bool magicAttack;

	[SerializeField]
	private float manaCost;

	[SerializeField]
	private float minAttackMultiplier;

	[SerializeField]
	private float maxAttackMultiplier;

	[SerializeField]
	private float minDefenseMultiplier;

	[SerializeField]
	private float maxDefenseMultiplier;

    /*void start()
    {
        if (owner.tag == "PlayerUnit")
        {
            ownerStats = this.owner.GetComponent<UnitStats>().localPlayerData;
        }
        else
        {
            EnemyStats ownerStats = this.owner.GetComponent<EnemyStats>();
        }
    }*/

    public void hit(GameObject target) {

        if (owner.tag == "PlayerUnit")
        {
            hitenemy();
        }
        else
        {
            hitplayer();
        }

        /*if (target.tag == "PlayerUnit")
        {
            targetStats = target.GetComponent<UnitStats>().localPlayerData;
        }
        else
        {
            EnemyStats targetStats = target.GetComponent<EnemyStats>();
        }*/


        void hitenemy()
        {
            ownerStats = this.owner.GetComponent<UnitStats>().localPlayerData;
            EnemyStats targetEnemyStats = target.GetComponent<EnemyStats>();

            if (ownerStats.mana >= this.manaCost)
            {
                float attackMultiplier = (Random.value * (this.maxAttackMultiplier - this.minAttackMultiplier)) + this.minAttackMultiplier;
                float damage = (this.magicAttack) ? attackMultiplier * ownerStats.magic : attackMultiplier * ownerStats.attack;

                float defenseMultiplier = (Random.value * (this.maxDefenseMultiplier - this.minDefenseMultiplier)) + this.minDefenseMultiplier;
                damage = Mathf.Max(0, damage - (defenseMultiplier * targetEnemyStats.defense));

                this.owner.GetComponent<Animator>().Play(this.attackAnimation);

                targetEnemyStats.receiveDamage(damage);

                ownerStats.mana -= this.manaCost;
            }
        }

        void hitplayer()
        {
            EnemyStats ownerEnemyStats = this.owner.GetComponent<EnemyStats>();
            //targetStats = target.GetComponent<UnitStats>().localPlayerData;
            UnitStats targetStats = target.GetComponent<UnitStats>();

            if (ownerEnemyStats.mana >= this.manaCost)
            {
                float attackMultiplier = (Random.value * (this.maxAttackMultiplier - this.minAttackMultiplier)) + this.minAttackMultiplier;
                float damage = (this.magicAttack) ? attackMultiplier * ownerEnemyStats.magic : attackMultiplier * ownerEnemyStats.attack;

                float defenseMultiplier = (Random.value * (this.maxDefenseMultiplier - this.minDefenseMultiplier)) + this.minDefenseMultiplier;
                damage = Mathf.Max(0, damage - (defenseMultiplier * targetStats.localPlayerData.defense));

                this.owner.GetComponent<Animator>().Play(this.attackAnimation);

                targetStats.receiveDamage(damage);

                ownerEnemyStats.mana -= this.manaCost;
            }
        }
    }
}

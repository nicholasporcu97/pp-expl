﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class UnitStats : MonoBehaviour {

    //public static UnitStats Instance;

    public Transform playerPosition;

    public int charaStat;

    public PlayerStatistics localPlayerData = new PlayerStatistics();

    [SerializeField]
	private Animator animator;

	[SerializeField]
	private GameObject damageTextPrefab;
	[SerializeField]
	private Vector2 damageTextPosition;

    /*void Awake()
    {
        if (Instance == null)
            Instance = this;

        if (Instance != this)
            Destroy(gameObject);

        GlobalControl.Instance.Player = gameObject;
    }*/

    void Start()
    {
        if(charaStat==0)
        {
            localPlayerData = GlobalControl.Instance.savedPlayerData;
        }
        else
        {
            localPlayerData = GlobalControl.Instance.savedPlayerData2;
        }
    }

	public void receiveDamage(float damage) {
		localPlayerData.health -= damage;
		animator.Play ("Hit");

		GameObject HUDCanvas = GameObject.Find ("HUDCanvas");
		GameObject damageText = Instantiate (this.damageTextPrefab, HUDCanvas.transform) as GameObject;
		damageText.GetComponent<Text> ().text = "" + damage;
		damageText.transform.localPosition = this.damageTextPosition;
		damageText.transform.localScale = new Vector2 (1.0f, 1.0f);

		if (localPlayerData.health <= 0) {
            GetComponent<StatsTurn>().dead = true;
			this.gameObject.tag = "DeadUnit";
			Destroy (this.gameObject);
		}
	}

	/*public void calculateNextActTurn(int currentTurn) {
		this.statsTurn.nextActTurn = currentTurn + (int)Math.Ceiling(100.0f / localPlayerData.speed);
	}*/

	/*public int CompareTo(object otherStats) {
        return nextActTurn.CompareTo(((UnitStats)otherStats).nextActTurn);
    }*/

	public void receiveExperience(float experience) {
		localPlayerData.currentExperience += experience;
        SavePlayer();
	}

    public void SavePlayer()
    {
        if (charaStat == 0) {
            GlobalControl.Instance.savedPlayerData = localPlayerData;
        }
        else
        {
            GlobalControl.Instance.savedPlayerData2 = localPlayerData;
        }
    }
}
